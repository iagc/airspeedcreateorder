<?php
namespace IAGC\airspeedcreateorder\Observer;

use Magento\Framework\Event\ObserverInterface;

class CustomerRegisterSuccess implements ObserverInterface
{
    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {

            $airSpeedData = array(
                "client_id" => '1', 
                "token_id" => '1', 
                "shipper" => '1', 
                "shipper_addr" => '222', 
                "shipper_city" => '1', 
                "shipper_prov" => '1', 
                "shipper_cont" => '1', 
                "consignee" => '1', 
                "consignee_addr" => '1', 
                "consignee_city" => '1', 
                "consignee_prov" => '1', 
                "consignee_cont" => '1', 
                "vendor_name" => '1', 
                "zipcode" => '1', 
                "waybill" => 'ABC' . str_pad(rand(130, 99999), 5, '0', STR_PAD_LEFT), 
                "ship_date" => '2018-04-17', 
                "order_no" => '1', 
                "declared_value" => '1', 
                "commodity" => '1', 
                "special_instruction" => '1', 
                "weight" => '1', 
                "payment_method" => '1', 
                "total_pieces" => '1', 
                "cod_amt" => '1'	
            );

            $res = $this->CallAPI("POST", "http://testapi.cfihost.com/api/AirSpeedOrder", $airSpeedData);
            // var_dump($res); die();
            // $this->logger->info($res);

        } catch (\Exception $e) {

            $this->logger->critical($e);

        }     

    }

    function CallAPI($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method)
        {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        curl_close($curl);

        return $result;


    }

}
