<?php
namespace IAGC\airspeedcreateorder\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProcessShipment implements ObserverInterface
{
    private $logger;

    /** @var Magento\Sales\Model\Order\ShipmentRepository */
    private $_shipmentRepository;

    /** @var Magento\Shipping\Model\ShipmentNotifier */
    private $_shipmentNotifier;

    /** @var Magento\Sales\Model\Order\Shipment\TrackFactory */
    private $_trackFactory;


    public function __construct(\Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\ShipmentNotifier $shipmentNotifier, 
        \Magento\Sales\Model\Order\ShipmentRepository $shipmentRepository, 
        \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory)
    {
        $this->logger = $logger;
        $this->_shipmentNotifier = $shipmentNotifier;
        $this->_shipmentRepository = $shipmentRepository;
        $this->_trackFactory = $trackFactory;
    }

    public function addTrack($shipment, $carrierCode, $description, $trackingNumber) 
    {
        /** Creating Tracking */
        /** @var Track $track */
        $track = $this->_trackFactory->create();
        $track->setCarrierCode($carrierCode);
        $track->setTitle($description);
        $track->setTrackNumber($trackingNumber);
        $shipment->addTrack($track);
        $this->_shipmentRepository->save($shipment);

    }

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {

            $shipment = $observer->getEvent()->getShipment();
            $order = $shipment->getOrder();

            $shippingDesc = strtolower($order->getShippingDescription());
            if (strpos($shippingDesc, "fastrack") > 0) {

                try {

                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
                    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                    $pdo = $resource->getConnection();

                     $sql= "INSERT INTO sequence_shipment_fastrack VALUES (NULL);";
                     $pdo->prepare($sql)->execute();
                     $waybill = str_pad((int)$pdo->lastInsertId(), 7, '0', STR_PAD_LEFT);
                     $sql = "INSERT INTO sales_shipment_track 
                         (parent_id, weight, qty, order_id, track_number, description, title, carrier_code)
                         VALUES 
                         (?,?,?,?,?,?,?,?);";

$pdo->prepare($sql)->execute([
    $shipment->getEntityId(), null, $order->getTotalQtyOrdered(),
    $order->getEntityId(), $waybill, null, 'Fastrack Delivery', 'custom'
]);

                } catch (\Exception $e) {

                    $this->logger->critical($e);
                }

            } elseif (strpos($shippingDesc, "cliqnship") > 0) {

                try {

                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
                    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                    $pdo = $resource->getConnection();

                     $sql= "INSERT INTO sequence_shipment_cliqnship VALUES (NULL);";
                     $pdo->prepare($sql)->execute();
                     $waybill = str_pad((int)$pdo->lastInsertId(), 7, '0', STR_PAD_LEFT);
                     $sql = "INSERT INTO sales_shipment_track 
                         (parent_id, weight, qty, order_id, track_number, description, title, carrier_code)
                         VALUES 
                         (?,?,?,?,?,?,?,?);";

$pdo->prepare($sql)->execute([
    $shipment->getEntityId(), null, $order->getTotalQtyOrdered(),
    $order->getEntityId(), $waybill, null, 'CliqNShip Delivery', 'custom'
]);

                } catch (\Exception $e) {

                    $this->logger->critical($e);
                }


            } else {

                // Production Prefix Code: “TM”
                // Test Prefix Code: “ST”
                // Format: “prefix code + 7 digits series number”
                $isLive = true;
                $prefix = 'ST';
                $url = 'https://api-booking.airspeed.com.ph/pickup_request/test_data/';
                $clientId = 'TONYMOLY';
                $tokenId = 'c47644b0ce78a5b36a16d816e7d533a4';

                if ($isLive) {
                    $prefix = 'TM';
                    $url = 'https://api-booking.airspeed.com.ph/pickup_request/production/';
                }

                if (!$isLive) return;

                $street = $order->getShippingAddress()->getStreet();
                $shipperStreet = $order->getShippingAddress()->getPrefix() . implode($street) . $order->getShippingAddress()->getSuffix();

                $shipper = 'TONYMOLY Philippines';
                $shipperAddr = '5148 Filmore St. corner Zobel Roxas Ave. Brangay Palanan';
                $shipperCity = 'Makati';
                $shipperProv = 'Metro Manila';

                $payment = $order->getPayment();
                $method = $payment->getMethodInstance();
                $methodTitle = $method->getTitle();
                $methodTitle = strtolower($methodTitle) == 'cash on delivery' ? 'COD' : $methodTitle;

                $weight = 0;
                $courier_category = array();

                foreach ($order->getAllItems() as $product) {
                    $weight += $product->getWeight();
                }

                $commodity = implode(array_unique($courier_category));
                $waybill = $prefix . str_pad((int)$shipment->getIncrementId(), 7, '0', STR_PAD_LEFT);

                $note = '';
                if (!is_null($order->getCustomerNote())) {
                    $note = $order->getCustomerNote();
                }

                $airSpeedData = array(
                    "client_id" => $clientId, 
                    "token_id" => $tokenId, 
                    "shipper" => $shipper,
                    "shipper_addr" => $shipperAddr,
                    "shipper_city" => $shipperCity, 
                    "shipper_prov" => $shipperProv,
                    "shipper_cont" => '(02)8361869',
                    "consignee" => str_replace("'", "", $order->getShippingAddress()->getName()), 
                    "consignee_addr" => str_replace("'", "", $shipperStreet),
                    "consignee_city" => str_replace("'", "", $order->getShippingAddress()->getCity()), 
                    "consignee_prov" => str_replace("'", "", $order->getShippingAddress()->getRegion()),
                    "consignee_cont" => str_replace("'", "", $order->getShippingAddress()->getTelephone()),
                    "vendor_name" => 'TONYMOLY Philippines', 
                    "zipcode" => $order->getShippingAddress()->getPostcode(), 
                    "waybill" => $waybill,
                    "ship_date" => date('Y-m-d'), 
                    "order_no" => $order->getRealOrderId(), 
                    "declared_value" => number_format($order->getGrandTotal(), 2, '.', ''), 
                    // "declared_value" => $order->getBaseTotalDue(), 
                    // "commodity" => $commodity, 
                    "commodity" => 'cosmetics', 
                    "special_instruction" => $note, 
                    "weight" => $weight, 
                    "payment_method" => $methodTitle, 
                    "total_pieces" => number_format($order->getTotalQtyOrdered(), 0, '.', ''), 
                    "cod_amt" => $methodTitle == 'COD' ? number_format($order->getGrandTotal(), 2, '.','') : 0
                );

                $this->logger->info($url);
                $res = $this->CallAPI("POST", $url, $airSpeedData);
                $this->logger->info(print_r($airSpeedData, true));
                $this->logger->info($res);
                $res = json_decode($res, true);
                $this->logger->info(isset($res['value']));

                if (isset($res['value'])) {
                    if (strpos(strtolower($res['value']), 'successfully') !== false) {
                        $this->addTrack($shipment, 'custom', 'AirSpeed Delivery', $waybill);
                    }
                }
            }

        } catch (\Exception $e) {

            $this->logger->critical($e);

        }     
    }

    function CallAPI($method, $url, $data = false)
    {
        $json = json_encode($data);
        $ch = curl_init($url);
        $jsonDataEncoded = json_encode($data);
        // echo $jsonDataEncoded;
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //For SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); //For SSL verification
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
        $content  =  curl_exec($ch);
        curl_close($ch);
        return $content;

    }

}
